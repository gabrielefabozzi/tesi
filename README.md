## gfThesis

gfThesis is an R package with functions and datasets referring to Gabriele Fabozzi's Master's Thesis.
All code is experimental and not optimized, as it is intended only for personal use at the moment.
All tests have been done on Windows, and there is no guarantee that code will work on other OS.
There are known issues with older versions of R.


## Installation

Use the `devtools` package through an internet connection

```r
devtools::install_gitlab("gabrielefabozzi/tesi")
library("gfThesis")
```